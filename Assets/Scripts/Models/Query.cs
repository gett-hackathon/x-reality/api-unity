﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Assets.Scripts.Models
{
    public class GqlQueryResult
    {
        [JsonProperty("data")]
        public GqlQueryData Data;
    }

    public class GqlQueryData
    {
        [JsonProperty("sharedApps")]
        public App[] SharedApps;

        [JsonProperty("appsByCategory")]
        public App[] AppsByCategory;

        [JsonProperty("allApps")]
        public App[] AllApps;
    }
}
