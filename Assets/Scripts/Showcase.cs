﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using Assets.Scripts.Models;
using UnityEngine;
using TMPro;

public class Showcase : MonoBehaviour
{
    public App Data;

    public TextMeshPro titleText;
    public WWWImage ShowCase;
    public WWWImage Icon;

	// Use this for initialization
	void Start ()
	{
	    StartCoroutine(Icon.SetImage(Data.LogoURL));
	    StartCoroutine(ShowCase.SetImage(Data.PreviewURLs[0]));
	    titleText.text = Data.Name;
	}
}
